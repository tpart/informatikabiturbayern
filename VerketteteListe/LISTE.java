package VerketteteListe;

public class LISTE {
    private LISTENELEMENT kopf;

    public LISTE() {
        kopf = new ABSCHLUSS();
    }

    public void einfügen(DATENELEMENT datenelement) {
        kopf.hintenHinzufügen(datenelement);
    }

    public void entfernen(DATENELEMENT datenelement) {
        kopf = kopf.datenEntfernen(datenelement);
    }
}