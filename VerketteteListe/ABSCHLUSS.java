package VerketteteListe;

public class ABSCHLUSS extends LISTENELEMENT {

    public KNOTEN hintenHinzufügen(DATENELEMENT datenelement) {
        KNOTEN k = new KNOTEN(datenelement);
        k.nachfolgerSetzen(this);
        return k;
    }

    public LISTENELEMENT datenEntfernen(DATENELEMENT datenelement) {
        // Datenelement konnte nicht gefunden werden.
        return this;
    }

    public void rekursivPrint() {
        // Ende der Liste wurde erreicht.
        return;
    }

    public int länge() {
        return 0;
    }
}
