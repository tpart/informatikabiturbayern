package VerketteteListe;

public abstract class LISTENELEMENT {
    // Rekursive Methoden
    public abstract KNOTEN hintenHinzufügen(DATENELEMENT datenelement);
    public abstract LISTENELEMENT datenEntfernen(DATENELEMENT datenelement);
    public abstract void rekursivPrint();
    public abstract int länge();
}
