package VerketteteListe;

public class KNOTEN extends LISTENELEMENT {
    private LISTENELEMENT nachfolger;
    private DATENELEMENT datenelement;

    public KNOTEN(DATENELEMENT datenelement) {
        this.datenelement = datenelement;

        /*
        Evtl. Nachfolgerknoten setzen.
        Gibt es keinen Nachfolger, so wird eine ABSCHLUSS-Instanz verwendet.
        */
    }

    public LISTENELEMENT nachfolgerGeben() {
        return nachfolger;
    }

    public void nachfolgerSetzen(LISTENELEMENT nachfolger) {
        this.nachfolger = nachfolger;
    }

    public DATENELEMENT datenelementGeben() {
        return datenelement;
    }

    public void datenelementSetzen(DATENELEMENT datenelement) {
        this.datenelement = datenelement;
    }

    public KNOTEN hintenHinzufügen(DATENELEMENT datenelement) {
        nachfolger = nachfolger.hintenHinzufügen(datenelement);
        return this;
    }

    public LISTENELEMENT datenEntfernen(DATENELEMENT datenelement) {
        if (this.datenelement.gleich(datenelement)) {
            return nachfolger;
        }
        nachfolger = nachfolger.datenEntfernen(datenelement);
        return this;
    }

    public void rekursivPrint() {
        datenelement.print();
        nachfolger.rekursivPrint();
    }

    public int länge() {
        return 1 + nachfolger.länge();
    }
}
