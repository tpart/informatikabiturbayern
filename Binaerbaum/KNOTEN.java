package Binaerbaum;

public class KNOTEN extends BAUMELEMENT {
    private BAUMELEMENT links, rechts;
    private DATENELEMENT inhalt;

    public KNOTEN(DATENELEMENT inhalt) {
        this.inhalt = inhalt;
    }

    public BAUMELEMENT einfuegen(DATENELEMENT daten) {
        if (inhalt.istKleiner(daten)) {
            links = links.einfuegen(daten);
        } else {
            rechts = rechts.einfuegen(daten);
        }
        return this;
    }

    public BAUMELEMENT suchen(DATENELEMENT daten) {
        if (inhalt.istGleich(daten)) {
            return this;
        } else if (inhalt.istKleiner(daten)) {
            return links.suchen(daten);
        } else {
            return rechts.suchen(daten);
        }
    }
}
