package Binaerbaum;

public class DATENELEMENT {
    public int wert; // Wert entspricht hier dem Schlüssel

    public DATENELEMENT(int wert) {
        this.wert = wert;
    }

    public boolean istKleiner(DATENELEMENT e) {
        return wert < e.wert;
    }

    public boolean istGleich(DATENELEMENT e) {
        return wert == e.wert;
    }
}
