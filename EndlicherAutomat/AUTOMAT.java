package EndlicherAutomat;

public class AUTOMAT {
    private static final int startzustand = 0;
    private int zustand = startzustand;
    private int endzustand = 3; // ggf. auch mehrere Endzustände (etwa mit Array möglich)

    private void zustandWechseln(char c) {
        switch (zustand) {
            case 0:
                switch (c) {
                    case '0':
                        zustand = 1;
                        break;
                    case '1':
                        zustand = 2;
                        break;
                }
                break;
            // etc.
        }
    }

    public boolean stringPrüfen(String s) {
        zustand = startzustand;
        for (int i = 0; i < s.length(); i++) {
            zustandWechseln(s.charAt(i));
        }
        return zustand == endzustand;
    }
}
