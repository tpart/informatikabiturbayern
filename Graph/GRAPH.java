package Graph;

public class GRAPH {
    private KNOTEN[] knoten;
    private boolean[][] adjazenzmatrix; // Bei gewichteten Graphen int[][]
    
    public GRAPH(int anzahl) {
        knoten = new KNOTEN[anzahl];
        adjazenzmatrix = new boolean[anzahl][anzahl];
    }

    public void tiefensucheStarten(KNOTEN start, KNOTEN ziel) throws Exception {
        for (int i = 0; i < knoten.length; i++) {
            knoten[i].besucht = false;
        }
        tiefensuche(start, ziel);
    }

    private void tiefensuche(KNOTEN start, KNOTEN ziel) throws Exception {
        int knotenIndex = knotenZuIndex(start);
        
        for (int i = 0; i < adjazenzmatrix[knotenIndex].length; i++) {
            if (adjazenzmatrix[knotenIndex][i] && !knoten[i].besucht) {
                tiefensuche(knoten[i], ziel);
            }
        }
    }

    public void kanteEinfügen(KNOTEN k1, KNOTEN k2, boolean gerichtet) throws Exception {
        int indexk1 = knotenZuIndex(k1);
        int indexk2 = knotenZuIndex(k2);

        adjazenzmatrix[indexk1][indexk2] = true;

        if (!gerichtet) {
            adjazenzmatrix[indexk2][indexk1] = true;
        }
    }

    public int knotenZuIndex(KNOTEN k) throws Exception {
        for (int i = 0; i < knoten.length; i++) {
            if (knoten[i] == k) {
                return i;
            }
        }
        throw new Exception("Knoten wurde nicht gefunden");
    }
}
