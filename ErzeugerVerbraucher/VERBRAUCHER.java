package ErzeugerVerbraucher;

public class VERBRAUCHER {
    private PUFFER puffer;

    public VERBRAUCHER(PUFFER puffer) {
        this.puffer = puffer; // Der gleiche Puffer wird von Erzeuger & Verbraucher verwendet
    }

    public void entnehmen() {
        Object objekt = puffer.entnehmen();
        System.out.println("Entnommen: " + objekt);
    }
}
