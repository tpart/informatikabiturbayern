package ErzeugerVerbraucher;

public class ERZEUGER {
    private PUFFER puffer;

    public ERZEUGER(PUFFER puffer) {
        this.puffer = puffer; // Der gleiche Puffer wird von Erzeuger & Verbraucher verwendet
    }

    public void erzeugen() throws Exception {
        // Dem Puffer wird ein neues Objekt zur Verfügung gestellt.
        // Beispiel: Eine E-Mail trifft ein, die nun zwischengespeichert werden muss.
        puffer.einfügen(new Object());
    }
}
