package ErzeugerVerbraucher;

public class PUFFER {
    private Object[] puffer;
    private int entnehmenIndex, einfügenIndex, anzahl;

    public PUFFER(int größe) {
        anzahl = 0;
        puffer = new Object[größe];
    }

    public synchronized void einfügen(Object element) throws Exception {
        if (anzahl == puffer.length) {
            throw new Exception("Puffer ist bereits voll");
        }
        puffer[einfügenIndex] = element;
        einfügenIndex = (einfügenIndex + 1) % puffer.length;
        anzahl++;
        notify();
    }

    public synchronized Object entnehmen() {
        Object objekt = puffer[entnehmenIndex];
        puffer[entnehmenIndex] = null;
        anzahl--;
        entnehmenIndex = (entnehmenIndex + 1) % puffer.length;
        return objekt;
    }
}
